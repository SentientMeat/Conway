#pragma once

#include "CPopulation.h"

enum class EWorldCmd						// commands received from the application layer
{
	KeyStop = 0,
	KeyStart = 1,
	KeyPause = 2,
	KeyResume = 3,
	MouseLButton = 10,
	MouseMButton = 11,
	MouseRButton = 12,
};


enum class EWorldState						// activity states of the world
{
	Idle = 0,
	Running = 1,
	Paused = 2,
};


class CWorld
{
public:
	CWorld();
	~CWorld();
	bool initialise(HWND hWnd);
	void think();
	bool passCommand(EWorldCmd cmd, int xp = 0, int yp = 0);
	EWorldState getState(void) { return m_state; }

private:
	void startWorld();
	void stopWorld();
	void pauseWorld();
	void resumeWorld();
	void msgBox(HWND hWnd, const std::string msg);

	_int64 m_ctrFreq;						// performance counter frequency [counts/sec]
	_int64 m_thinkTime;						// world update interval [counts]
	_int64 m_tStart;						// current world time [counts]
	_int64 m_tNow;							// current world time [counts]
	_int64 m_tNextThink;					// next world update time [counts]
	CPopulation m_population;				// the cellular automata
	EWorldState m_state;					// game activity state
	HWND m_hWnd;							// handle to the main window
};
