#include "stdafx.h"
#include "CPopulation.h"

static const flag maskActive = 0x1;
static const flag maskNext = 0x2;
static const WORD cSize = 4;
static const WORD cLoLim = 2;
static const WORD cHiLim = 3;


//----------------------------------------------------------------------------------------------------
CPopulation::CPopulation()
{
	m_nFrames = 0;
}


CPopulation::~CPopulation()
{
	clearCells();
}


//----------------------------------------------------------------------------------------------------
// PURPOSE: Perform the one-time instance initialisation required to run the object.
void CPopulation::initialise(HWND hWnd)
{
	// Determine the drawing bounds of the window.
	RECT rect;
	GetClientRect(hWnd, &rect);
	int winWidth = rect.right - rect.left;
	int winHeight = rect.bottom - rect.top;
	
	// Resize the population array to fit these bounds.
	m_sizeX = (UINT)floor(winWidth / cSize);
	m_sizeY = (UINT)floor(winHeight / cSize);
	m_cells.resize(m_sizeX);
	for (UINT n = 0; n < m_sizeX; ++n)
	{
		m_cells[n].resize(m_sizeY);
	}
}


//----------------------------------------------------------------------------------------------------
// PURPOSE: Clear the array of cells to an inactive/empty state.
void CPopulation::clearCells()
{
	m_nFrames = 0;
	for (UINT y = 0; y < m_sizeY; ++y)
	{
		for (UINT x = 0; x < m_sizeX; ++x)
		{
			m_cells[x][y] = 0;
		}
	}
}


//----------------------------------------------------------------------------------------------------
// PURPOSE: Optional/test function to set an initial configuration of some cells.
void CPopulation::seedCells(HWND hWnd)		//TODO: Create functions for the various shapes | Implement an editor
{
	for (UINT x = 50; x < 100; x+=3)
	{
		for (UINT y = 50; y < 100; y+=3)
		{
			putBlock(x, y, false);
		}
	}
}


//----------------------------------------------------------------------------------------------------
// PURPOSE: Apply the Game Of Life rules to determine the next generation of cells.
void CPopulation::runFrame()
{
	// Calculate the states of the next generation of cells.
	UINT xl, xr, yl, yu;
	byte num;
	for (UINT y = 1; y < m_sizeY-1; ++y)
	{
		for (UINT x = 1; x < m_sizeX-1; ++x)
		{
			xl = x - 1;
			xr = x + 1;
			yl = y - 1;
			yu = y + 1;
			num = 0;
			num += (m_cells[xl][yl] & maskActive) ? 1 : 0;
			num += (m_cells[xl][y]  & maskActive) ? 1 : 0;
			num += (m_cells[xl][yu] & maskActive) ? 1 : 0;
			num += (m_cells[x][yl]  & maskActive) ? 1 : 0;
			num += (m_cells[x][yu]  & maskActive) ? 1 : 0;
			num += (m_cells[xr][yl] & maskActive) ? 1 : 0;
			num += (m_cells[xr][y]  & maskActive) ? 1 : 0;
			num += (m_cells[xr][yu] & maskActive) ? 1 : 0;

			if ((num < cLoLim) || (num > cHiLim) || (!(m_cells[x][y] & maskActive) && (num == cLoLim)))
				m_cells[x][y] &= ~maskNext;
			else 
				m_cells[x][y] |= maskNext;
		}
	}

	// Make the next generations's state the currently active one.
	for (UINT y = 1; y < m_sizeY - 1; ++y)
	{
		for (UINT x = 1; x < m_sizeX - 1; ++x)
		{
			m_cells[x][y] = (m_cells[x][y] & maskNext) ? maskActive : 0;
		}
	}

	++m_nFrames;
}


//----------------------------------------------------------------------------------------------------
// PURPOSE: Display the current population in the specific window.
void CPopulation::drawFrame(HWND hWnd)
{
	// Setup the GDI resources.
	RECT rect;
	GetClientRect(hWnd, &rect);
	int winWidth = rect.right - rect.left;
	int winHeight = rect.bottom - rect.top;

	HDC hdc = GetDC(hWnd);
	HDC hdcMem = CreateCompatibleDC(hdc);
	HBITMAP bmpMem = CreateCompatibleBitmap(hdc, winWidth, winHeight);
	SelectObject(hdcMem, bmpMem);

	// Loop through the grid and draw the active cells.
	WORD box = cSize + 1;
	UINT xp;
	UINT yp;
	for (UINT y = 0; y < m_sizeY; ++y)
	{
		for (UINT x = 0; x < m_sizeX; ++x)
		{
			if (m_cells[x][y] & maskActive)
			{
				xp = x * cSize;
				yp = y * cSize;
				Rectangle(hdcMem, xp, yp, xp + box, yp + box);
			}
		}
	}

	// Copy the back buffer to the window and clean up the GDI resources.
	BitBlt(hdc, 0, 0, winWidth, winHeight, hdcMem, 0, 0, SRCCOPY);

	DeleteDC(hdcMem);
	DeleteObject(bmpMem);
	DeleteDC(hdc);
	ReleaseDC(hWnd, hdc);
}


//----------------------------------------------------------------------------------------------------
// PURPOSE: Handles a request from the world to draw a single cell.
void CPopulation::drawPoint(HWND hWnd, int xp, int yp)
{
	// Sanity-check.
	if ((xp < 0) || (yp < 0))
		return;

	// Convert the given screen coordinates to the cooresponding cell location.
	UINT x = (UINT)(xp / cSize);
	UINT y = (UINT)(yp / cSize);
	if ((x > m_sizeX - 1) || (y > m_sizeY - 1))
		return;

	// Update the cell.
	m_cells[x][y] = true;
	drawUpdatedRegion(hWnd);
	return;
}


//----------------------------------------------------------------------------------------------------
// PURPOSE: Handles a request from the world to draw cells in a block.
void CPopulation::drawBlock(HWND hWnd, int xp, int yp)
{
	// Sanity-check.
	if ((xp < 0) || (yp < 0))
		return;

	// Convert the given screen coordinates to the corresponding cell location.
	UINT x = (UINT)(xp / cSize);
	UINT y = (UINT)(yp / cSize);

	// Update the cells.
	putBlock(x, y, true);
	drawUpdatedRegion(hWnd);
	return;
}


//----------------------------------------------------------------------------------------------------
// PURPOSE: Handles a request from the world to delete a block of cells.
void CPopulation::deleteBlock(HWND hWnd, int xp, int yp)
{
	// Sanity-check.
	if ((xp < 0) || (yp < 0))
		return;

	// Convert the given screen coordinates to the cooresponding cell location.
	UINT x = (UINT)(xp / cSize);
	UINT y = (UINT)(yp / cSize);
	
	// Update the cells.
	putBlock(x, y, true, false);
	drawUpdatedRegion(hWnd);
	return;
}


//----------------------------------------------------------------------------------------------------
// PURPOSE: Display the specified string in a Windows message box.
void CPopulation::msgBox(HWND hWnd, const std::string msg)
{
	MessageBoxA(hWnd, msg.c_str(), "Message", MB_OK | MB_ICONINFORMATION);
}


//----------------------------------------------------------------------------------------------------
// PURPOSE: Draw the updated region of the cell population.
void CPopulation::drawUpdatedRegion(HWND hWnd)
{
	//TODO only draw updated region
	drawFrame(hWnd);
}


//----------------------------------------------------------------------------------------------------
// PURPOSE: Change the state of a group of cells in a filled or hollow block configuration.
//          set:T => active | set:F => delete
void CPopulation::putBlock(UINT x, UINT y, bool isSolid, bool set)		//TODO: can specify size
{
	//Do the pre-calcs.
	flag status = (set) ? (maskActive | maskNext) : 0;
	
	UINT xl = x - 1;
	UINT xm = x;
	UINT xr = x + 1;

	UINT yl = y - 1;
	UINT ym = y;
	UINT yu = y + 1;

	// Clip the border region.
	if (xl < 1) xl = 1;
	if (xm < 1) xm = 1;
	if (xr < 1) xr = 1;

	if (yl < 1) yl = 1;
	if (ym < 1) ym = 1;
	if (yu < 1) yu = 1;

	if (xl > m_sizeX - 2) xl = m_sizeX - 2;
	if (xm > m_sizeX - 2) xm = m_sizeX - 2;
	if (xr > m_sizeX - 2) xr = m_sizeX - 2;

	if (yl > m_sizeY - 2) yl = m_sizeY - 2;
	if (ym > m_sizeY - 2) ym = m_sizeY - 2;
	if (yu > m_sizeY - 2) yu = m_sizeY - 2;

	// Set the cell states.
	m_cells[xl][yl] = status;
	m_cells[xl][ym] = status;
	m_cells[xl][yu] = status;
	m_cells[xm][yl] = status;
	m_cells[xm][ym] = (isSolid) ? status : 0;
	m_cells[xm][yu] = status;
	m_cells[xr][yl] = status;
	m_cells[xr][ym] = status;
	m_cells[xr][yu] = status;
}