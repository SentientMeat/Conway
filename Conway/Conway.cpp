// Conway.cpp : Defines the entry point for the application.

#include "stdafx.h"
#include "Conway.h"
#include "CWorld.h"

// Constants:
static const int W_X = 400;
static const int W_Y = 20;
static const int W_WIDTH = 1020;
static const int W_HEIGHT = 1050;

// Global variables:
HINSTANCE g_hInst;								// current instance
WCHAR g_szTitle[MAX_LOADSTRING];				// the title bar text
WCHAR g_szWindowClass[MAX_LOADSTRING];			// the main window class name
CWorld g_world;									// the global world instance

// Forward declarations of functions included in this code module:
ATOM MyRegisterClass(HINSTANCE hInstance);
HWND InitInstance(HINSTANCE, int, bool, int, int, int, int);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK About(HWND, UINT, WPARAM, LPARAM);


//----------------------------------------------------------------------------------------------------
// PURPOSE: The application's Main function.
int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

	// Initialise the application.
	LoadStringW(hInstance, IDS_APP_TITLE, g_szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_CONWAY, g_szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);
	HWND hWnd;							// handle of the main window
	if (NULL == (hWnd = InitInstance(hInstance, nCmdShow, false, W_X, W_Y, W_WIDTH, W_HEIGHT)))
        return FALSE;

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_CONWAY));
	MSG msg;
	msg.message = ~WM_QUIT;
	srand((unsigned)time(NULL));

	// initialise the world object.
	if (!g_world.initialise(hWnd))
		return FALSE;

	// Run the main application loop until the window is closed:
	while (msg.message != WM_QUIT)
	{
		// Check for and handle Windows messages in the queue.
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{			
			if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		// Advance the state of the world.
		g_world.think();
	}

    return (int) msg.wParam;
}


//----------------------------------------------------------------------------------------------------
// PURPOSE: Configure and register the window class.
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;
    wcex.cbSize			= sizeof(WNDCLASSEX);
    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_CONWAY));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground	= NULL;		//TODO: (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName	= MAKEINTRESOURCEW(IDC_CONWAY);
    wcex.lpszClassName  = g_szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}


//----------------------------------------------------------------------------------------------------
// PURPOSE: Create and display the main window.
HWND InitInstance(HINSTANCE hInstance, int nCmdShow, bool isFullscreen, int x, int y, int width, int height)
{
	g_hInst = hInstance;			// save the instance handle in a global variable
	DWORD dwStyle = 0;
	DWORD dwExStyle = 0;
	HWND hWnd = nullptr;

	// Configure the window for fullscreen or windowed mode.
	if (isFullscreen)
	{		
		dwStyle = WS_POPUP;
		dwExStyle = WS_EX_APPWINDOW | WS_EX_TOPMOST;
		hWnd = CreateWindowEx(dwExStyle, g_szWindowClass, NULL, dwStyle, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), nullptr, nullptr, hInstance, nullptr);
	}
	else
	{
		dwStyle = WS_OVERLAPPED | WS_BORDER | WS_MINIMIZEBOX | WS_SYSMENU | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
		dwExStyle = WS_EX_APPWINDOW;
		hWnd = CreateWindowEx(dwExStyle, g_szWindowClass, g_szTitle, dwStyle, x, y, width, height, nullptr, nullptr, hInstance, nullptr);
		
		// Resize the window to account for the space taken by the title bar and border.
		RECT windowRect;
		windowRect.left = 0;
		windowRect.right = width;
		windowRect.top = 0;
		windowRect.bottom = height;
		AdjustWindowRectEx(&windowRect, dwStyle, FALSE, dwExStyle);
	}

	if (!hWnd)
		return FALSE;

	// Display the widow.
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return hWnd;
}


//----------------------------------------------------------------------------------------------------
// PURPOSE: Process messages for the main window.
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
		int wmId;

		// Process command messages:
		case WM_COMMAND:
			wmId = LOWORD(wParam);
			switch (wmId)
			{
				// accelerator keystrokes...
				case IDK_START:
					g_world.passCommand(EWorldCmd::KeyStart);
					break;

				case IDK_STOP:
					g_world.passCommand(EWorldCmd::KeyStop);
					break;

				case IDK_PAUSE:
					g_world.passCommand(EWorldCmd::KeyPause);
					break;

				case IDK_RESUME:
					g_world.passCommand(EWorldCmd::KeyResume);
					break;

				case IDK_EXIT:
					DestroyWindow(hWnd);
					break;

				// menu items...
				case IDM_ABOUT:
					DialogBox(g_hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
					break;
			
				case IDM_EXIT:
					DestroyWindow(hWnd);
					break;
				
				// all other commands...
				default:
					return DefWindowProc(hWnd, message, wParam, lParam);
			}
			break;

		// Process mouse button presses:
		case WM_LBUTTONDOWN:
			g_world.passCommand(EWorldCmd::MouseLButton, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
			break;

		case WM_MBUTTONDOWN:
			g_world.passCommand(EWorldCmd::MouseMButton, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
			break;

		case WM_RBUTTONDOWN:
			g_world.passCommand(EWorldCmd::MouseRButton, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
			break;

		// Process mouse movement:
		case WM_MOUSEMOVE:
			wmId = LOWORD(wParam);
			switch (wmId)
			{
				case (MK_LBUTTON):
					g_world.passCommand(EWorldCmd::MouseLButton, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
					break;

				case (MK_RBUTTON):
					g_world.passCommand(EWorldCmd::MouseRButton, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
					break;
			}
			break;
		
		// Paint the main window:
		case WM_PAINT:
			PAINTSTRUCT ps;
			HDC hdc;
			hdc = BeginPaint(hWnd, &ps);

			if (g_world.getState() == EWorldState::Idle)
			{
				RECT rect;
				GetClientRect(hWnd, &rect);
				int winWidth = rect.right - rect.left;
				int winHeight = rect.bottom - rect.top;

				HBRUSH hbBlack;
				hbBlack = CreateSolidBrush(BLACK);
				SelectObject(hdc, hbBlack);
				Rectangle(hdc, 0, 0, winWidth, winHeight);
				DeleteObject(hbBlack);
			}

			EndPaint(hWnd, &ps);
			break;

		// Prevent background flickering:
		case WM_ERASEBKGND:
			return 1;
		
		// Post a quit message and return:
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		
		// Handle all other messages:
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

//----------------------------------------------------------------------------------------------------
// PURPOSE: Handle messages for the About box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
		case WM_INITDIALOG:
			return (INT_PTR)TRUE;

		case WM_COMMAND:
			if ((LOWORD(wParam) == IDOK) || (LOWORD(wParam) == IDCANCEL))
			{
				EndDialog(hDlg, LOWORD(wParam));
				return (INT_PTR)TRUE;
			}
			break;
    }

    return (INT_PTR)FALSE;
}
