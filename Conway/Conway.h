#pragma once

#include "resource.h"

// Application attributes
#define MAX_LOADSTRING	100

// 32-bit colour hex codes
#define BLACK			0x00000000		
#define GREEN			0x0010FF10
#define DARKGREEN		0x0000A000
#define RED				0x000000FF
#define DARKRED			0x00000090
#define YELLOW			0x0000FFFF
#define SILVER			0x00DDDDE0
#define WHITE			0x00FFFFFF
