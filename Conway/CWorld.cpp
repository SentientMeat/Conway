#include "stdafx.h"
#include "CWorld.h"

static const double FrameTime = 0.05;		// time between world updates [sec] => change this to slow down/speed up the game


//----------------------------------------------------------------------------------------------------
CWorld::CWorld()
{
	m_state = EWorldState::Idle;
}


CWorld::~CWorld()
{
	m_state = EWorldState::Idle;
}

//----------------------------------------------------------------------------------------------------
// PURPOSE: Handle commands, key and mouse presses passed in from the application.
bool CWorld::passCommand(EWorldCmd cmd, int xp, int yp)
{
	std::string msg;
	switch (cmd)
	{
		case EWorldCmd::KeyStop:
			if ((m_state == EWorldState::Running) || (m_state == EWorldState::Paused))
				stopWorld();
			break;
	
		case EWorldCmd::KeyStart:
			if (m_state == EWorldState::Idle)
				startWorld();
			break;

		case EWorldCmd::KeyPause:
			if (m_state == EWorldState::Running)
				pauseWorld();
			break;

		case EWorldCmd::KeyResume:
			if (m_state == EWorldState::Paused)
				resumeWorld();
			break;

		case EWorldCmd::MouseLButton:
			m_population.drawBlock(m_hWnd, xp, yp);
			break;

		case EWorldCmd::MouseMButton:
			m_population.drawPoint(m_hWnd, xp, yp);
			break;

		case EWorldCmd::MouseRButton:
			m_population.deleteBlock(m_hWnd, xp, yp);
			break;

		default:
			return false;
	}

	return true;
}


//----------------------------------------------------------------------------------------------------
// PURPOSE: Perform the one-time instance initialisation required to run the object.
bool CWorld::initialise(HWND hWnd)
{
	// Setup the timer functionality.
	if (!QueryPerformanceFrequency((LARGE_INTEGER*)&m_ctrFreq))		// initialise the performance counter
		return false;

	m_thinkTime = (_int64)(FrameTime * m_ctrFreq);
	m_hWnd = hWnd;													// store the window's handle
	
	// Setup the m_population of cells.
	m_population.initialise(m_hWnd);
	m_population.seedCells(m_hWnd);
	m_population.drawFrame(m_hWnd);
	return true;
}


//----------------------------------------------------------------------------------------------------
// PURPOSE: Command to start the world running.
void CWorld::startWorld()
{
	m_state = EWorldState::Running;

	QueryPerformanceCounter((LARGE_INTEGER*)&m_tNow);
	m_tStart = m_tNow;
	m_tNextThink = m_tNow + m_thinkTime;
}


//----------------------------------------------------------------------------------------------------
// PURPOSE: Command to stop the world running.
void CWorld::stopWorld()
{
	m_state = EWorldState::Idle;

	/*QueryPerformanceCounter((LARGE_INTEGER*)&m_tNow);
	double elapsedTime = (double)(m_tNow - m_tStart) / (double)m_ctrFreq;
	UINT nFrames = m_population.getFrames();
	double fps = nFrames / elapsedTime;
	std::string msg = "Simulation stopped after " + std::to_string(nFrames) + " frames\n" + "Elapsed time = " + std::to_string(elapsedTime) + " secs\n" + "Average frames / sec = " + std::to_string(fps);
	msgBox(m_hWnd, msg);*/	//DEBUG - restore

	m_population.clearCells();
	m_population.drawFrame(m_hWnd);
}


//----------------------------------------------------------------------------------------------------
// PURPOSE: Command to temporarily suspend the world's running.
void CWorld::pauseWorld()
{
	m_state = EWorldState::Paused;

	std::string msg = "Frames = " + std::to_string(m_population.getFrames());
	msgBox(m_hWnd, msg);
}


//----------------------------------------------------------------------------------------------------
// PURPOSE: Command to resume the world's running.
void CWorld::resumeWorld()
{
	m_state = EWorldState::Running;

	QueryPerformanceCounter((LARGE_INTEGER*)&m_tNow);
	m_tNextThink = m_tNow + m_thinkTime;
}


//----------------------------------------------------------------------------------------------------
// PURPOSE: Process and advance the world's state by one frame (timer-based).
void CWorld::think()
{
	// Check for exit states.
	if ((m_state == EWorldState::Idle) || (m_state == EWorldState::Paused))
		return;

	QueryPerformanceCounter((LARGE_INTEGER*)&m_tNow);
	if (m_tNow < m_tNextThink)
		return;

	// Evolve the m_population and display the results.
	m_population.runFrame();
	m_population.drawFrame(m_hWnd);
	m_tNextThink = m_tNow + m_thinkTime;
}


//----------------------------------------------------------------------------------------------------
// PURPOSE: Display the specified string in a Windows message box.
void CWorld::msgBox(HWND hWnd, const std::string msg)
{
	MessageBoxA(hWnd, msg.c_str(), "Message", MB_OK | MB_ICONINFORMATION);
}
