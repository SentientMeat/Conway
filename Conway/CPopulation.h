#pragma once

typedef unsigned char flag;
typedef unsigned char byte;


class CPopulation
{
public:
	CPopulation();
	~CPopulation();

	void initialise(HWND hWnd);
	void clearCells();
	void seedCells(HWND hWnd);
	void runFrame();
	void drawFrame(HWND hWnd);
	void drawPoint(HWND hWnd, int xp, int yp);
	void drawBlock(HWND hWnd, int xp, int yp);
	void deleteBlock(HWND hWnd, int xp, int yp);
	void msgBox(HWND hWnd, const std::string msg);
	UINT getFrames() { return m_nFrames; }

private:
	void drawUpdatedRegion(HWND hWnd);
	void putBlock(UINT x, UINT y, bool isSolid = true, bool set = true);

	std::vector<std::vector<flag>> m_cells;
	UINT m_sizeX;
	UINT m_sizeY;
	UINT m_nFrames;
};
