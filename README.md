# Conway's Game Of Life
A Win32 version of the classic cellular automaton.

## Author
Chris Walker

## Game Notes

**Use the mouse buttons to manipulate cells:**
* LMB = Activate (add) a 3x3 block of cells
* MMB = Activate (add) a single cell
* RMD = Deactivate (delete) a 3x3 block of cells

**Key guide:**
* S = Start
* P = Pause
* R = Resume
* X = Stop
* Q = Exit

## Dev Notes

The solution and project files are for Microsoft Visual Studio 2017.

To adjust the speed at which the game runs, change the value of **_FrameTime_** in the file _CWorld.cpp_. Setting it to a value of 0.0 will give the maximum speed.

## Licence
Public Domain license � you may do with this code as you wish.
